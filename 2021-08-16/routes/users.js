var express = require('express');
var router = express.Router();
const { User } = require('../models');

/* GET users listing. */
router.get('/', async function(req, res, next) {
  const users = await User.findAll();
  console.log(users);

  res.render('listUsers', { title: 'Express', users: users });
  // res.send('respond with a resource');
});

router.post('/', async (req, res, next) => {
  if (req.body.etunimi) {
    await User.create({ firstName: req.body.etunimi, lastName: req.body.sukunimi });
  }
  const users = await User.findAll();

  res.render('listUsers', { title: 'Express', users: users });
});

module.exports = router;

